import { ajax } from "./ajax.js";

export class Capsule {
	tag = null;
	tagTemplate = null;
    attributes = [];
    
    currentAttributes = [];
    currentConditionals = [];
	
	node = null;
	template = null;
	tagName = null;
	parent = null;
	children = [];
	element = null;
	toChange = false;
	masked = null;
    transclude = null;

    initialiced = false;
    visible = true;
    removed = false;

    formed = false;


	constructor (node) {
		if (node.nodeType == 1) {
			this.type = 'Element';
		}
		if (node.nodeType == 2) {
			this.type = 'Comment';
		}
		if (node.nodeType == 3) {
			this.type = 'Text';
		}
		
		this.template = node;
		if (this.template instanceof Element) {
			this.tagName = node.tagName;
		}
	}

	generate (scope) {
        if (this.element) {
            return;
        }
        if (!this.toChange) {
            this.element = this.template;
            if (this.parent) {
                this.parent.element.appendChild (this.element);
            }
            
        } else {
            this.element = this.template.cloneNode();
            if (this.parent) {
                this.parent.element.appendChild (this.element);
            }
            
            if (this.tag) {
                this.currentTag = new this.tag.tag.create();
                this.currentTag.capsule = this;
                let clone = this.tag._template.cloneNode(true);
                let child;
                while (child = clone.firstChild) {
                    clone.removeChild(child);
                    this.append (this.app.map (child, this.app));
                }
            }

            scope = this.currentTag || scope;
            
            if (this.attributes.length) {
                this.createAttributes (scope);
            }
        }
    }

    hide (scope) {
        if (!this.element) {
            return;
        }
        for (let i in this.children) {
            this.children[i].hide();
        }
        //this.children = [];
        if (this.toChange) {
            if (this.currentTag) {
                if (this.currentTag.$destroy)
                    this.currentTag.$destroy();
                this.currentTag = null;
            }
            
            /*for (let i in this.attributes) {
                if (this.attributes[i].$destroy)
                    this.attributes[i].$destroy();
            }
            this.attributes = [];*/
        }
        if (this.parent.element) {
            this.parent.element.removeChild (this.element);
        }
        this.element = null;
        
        //return this.template;
    }

    remove (scope) {
        this.visible = false;
        this.hide ();
        this.parent.children.splice (
            this.parent.children.indexOf (this),
            1
        );
    }

    initialice () {
        if (this.currentTag) {
            if (!this.currentTag._initialiced) {
                if (this.currentTag.$init) {
                    this.currentTag.$init();
                }
                
                this.currentTag._initialiced = true;
            }
        }
        if (this.currentAttributes.length) {
            for (let i in this.currentAttributes) {
                if (!this.currentAttributes[i]._initialiced) {
                    if (this.currentAttributes[i].$init) {
                        this.currentAttributes[i].$init();
                    }
                    
                    this.currentAttributes[i]._initialiced = true;
                }
            }
        }
        
    }

    initialiceConditionals () {
        if (this.currentConditionals.length) {
            for (let i in this.currentConditionals) {
                if (!this.currentConditionals[i]._initialiced) {
                    if (this.currentConditionals[i].$init) {
                        this.currentConditionals[i].$init();
                    }
                    this.currentConditionals[i]._initialiced = true;
                }
            }
        }
    }

    sweep (scope) {
        this.bootstrap(scope);
        if (!this.formed) {
            return;
        }
        
        if (this.removed) {
            this.remove()
            return;
        }

        if (this.visible) {
            this.generate (scope);
        } else {
            this.hide(scope);
        }


        this.initialiceConditionals();
        let last = this.visible;
        this.renderConditionals(scope);
        if (last != this.visible) {
            return;
        }
        if (!this.visible) return;
        scope = this.currentTag || scope;
        
        
        for (let i = 0; i < this.children.length; i++) {
            this.children[i].sweep(scope);
        }
        this.initialice (scope);
        this.render(scope);
        
        
    }

    loadTag () {
        let tagTemplate = this.tag;
        if (!tagTemplate.tag.ruta) {
            throw 'No rute selected in component';
        }

        // state empty
        if (tagTemplate.state == 0) {
            tagTemplate.state = 1; // state searching
            
            ajax.send (tagTemplate.tag.ruta, 'get', {})
            .then ((xhr) => {
                tagTemplate.state = 3;
                let div = document.createElement ('DIV');
                div.innerHTML = xhr.responseText;
                
                tagTemplate._template = div;
                tagTemplate.state = 2;
                this.app.aplicar();
                this.app.aplicar();
            })
            .catch ((data) => {
                tagTemplate.state = 3;
            });
        } else if (tagTemplate.state == 1) {
        } else if (tagTemplate.state == 3) {
        } else {
        }
        
    }

    createAttributes (scope) {
        console.log (this.currentAttributes.length + this.currentConditionals.length, this.attributes.length);
        if (this.currentAttributes.length + this.currentConditionals.length == this.attributes.length) {
            return;
        }
        this.currentAttributes = [];
        this.currentConditionals = [];
        for (let i in this.attributes) {
            let attrib = new this.attributes[i].create();
            attrib.element = this.element;
            attrib.capsule = this;
            attrib.nodeElement = attrib.capsule;
            attrib.$scope = scope;
            attrib.name = this.attributes[i].nombre;
            if (!this.app) {
                //console.log (this);
            }
            attrib.app = this.app;
            attrib.value = this.element.getAttribute(this.attributes[i].nombre);
            if (this.attributes[i].conditional) {
                this.currentConditionals.push(attrib);
            } else {
                this.currentAttributes.push(attrib);
            }
            if (attrib.controller) {
                attrib.controller();
            }
            
        }
    }

    bootstrap (scope) {
        if (this.formed) {
            return;
        }
        if (!this.toChange) {
            this.formed = true;
            return;
        }

        if (this.tag) {
            this.loadTag();
            if (this.tag.state == 3) {
                throw 'The route have been not found';
            }
            if (this.tag.state == 2) {
                this.formed = true;
            }
        } else {
            this.formed = true;
        }
    }

    renderConditionals (scope) {
        if (this.currentConditionals.length > 0) {
			for (let i in this.currentConditionals) {
                //console.log (this.currentConditionals[i]._initialiced)
                if (this.currentConditionals[i].$render && 
                    this.currentConditionals[i]._initialiced) {
                    
					this.currentConditionals[i].$render();
				}
			}
		}
    }

	render (scope) {
		if (this.masked) {
			if (this.masked instanceof Array) {
				for (let i in this.masked) {
					this.element.setAttribute (
						this.app.enmascararNotacion.call (scope, this.template.getAttribute (nodeElement.masked[i]))
					);
				}
			} else {
				this.element.data = this.app.enmascararNotacion.call (scope, this.template.data);
			}
		}
		if (this.currentTag && this.currentTag._initialiced) {
			if (this.currentTag.$render) {
				this.currentTag.$render();
			}
		}
		if (this.currentAttributes.length) {
			for (let i in this.currentAttributes) {
				if (this.currentAttributes[i].$render && this.currentAttributes[i]._initialiced) {
					this.currentAttributes[i].$render();
				}
			}
		}
	}

	append (child) {
        if (!(child instanceof Capsule)) {
            throw 'Must append a child';
        }
		if (this.children.indexOf (child) >= 0) {
			return;
        }
        
        child.app = this.app;
        if (!this.app) {
            //console.log ('alasjdlaksjd')
        }
		child.parent = this;
		this.children.push (child);
	}
}