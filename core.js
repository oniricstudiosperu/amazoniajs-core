import { Capsule } from "./capsule.js";
import { ajax } from "./ajax.js";

var amazonia = {
	apps: []
}

export class Etiqueta {
	_initialiced = false;
	constructor () { }
}

export class Atributo {
	_initialiced = false;
	constructor () { }
}


amazonia.exceptions = {
	inconsistentParentness: 'There are an inconsistence in parentness',
	inconsistentParentness2: 'There are an inconsistence in parentness2',
	noRouteSelected: 'No rute selected in component',
	notFoundRoute: 'The route have been not found'
}

amazonia.states = {
	standBy: 1,
	decorating: 2,
	decorateEnd: 3
};

amazonia.getQueryVariable = function (variable) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		if(pair[0] == variable) {
			return pair[1];
		}
	}
	return undefined;
 }

export class Frame {

	constructor(obj = null) {
		obj = obj || {};
		this.etiquetas = [];
		this.atributos = [];
		this.modules = [];
		this.modules.push (this);
		if (obj.componentes) {
			for (let i in obj.componentes) {
				this.appendTag (obj.componentes[i]);
			}
		}
		if (obj.atributos) {
			for (let i in obj.atributos) {
				this.appendAttribute (obj.atributos[i]);
			}
		}
		if (obj.modules) {
			for (let j in obj.modules) {
				for (let i in this.modules) {
					if (this.modules[i] instanceof obj.modules[j]) {
						throw { message: 'The module of type ' + obj.modules[j] + ' already exists in ' + this.constructor.name };
					}
				}
				this.modules.push (new obj.modules[j]());
			}
		}
		for (let i in this.modules) {
			if (this.modules[i] != this) {
				for (let j in this.modules[i].atributos) {
					this.appendAttribute (this.modules[i].atributos[j]);
				}
				for (let j in this.modules[i].componentes) {
					this.appendTag (this.modules[i].componentes[j]);
				}
			}
		}
		this.state = amazonia.states.standBy;
		this.elementosDOM = [];
		this.atributosDOM = [];
		this.alteredDOM = [];

		// Los componentes tendrán un ciclo de vida
		this.componentes = [];
		this.templates = [];	
	}

	appendTag (tag) {
		for (let i in this.etiquetas) {
			if (this.etiquetas[i] == tag.nombre) {
				throw 'The tag name "' + tag.nombre + '" is duplicated';
			}
		}
		this.etiquetas.push (tag);
	}

	appendAttribute (attribute) {
		for (let i in this.atributos) {
			if (this.atributos[i] == attribute.nombre) {
				throw 'The tag name "' + attribute.nombre + '" is duplicated';
			}
		}
		this.atributos.push (attribute);
	}

	captureWatch (attribute) {
		let value = attribute.value;

		let functMod = function (asign) {
			let capturedText;
			eval (value + '=' + JSON.stringify(asign));
			
			return capturedText;
		}
		attribute.asignValue = function (asign) {
			functMod.call (attribute.$scope, asign);
		}
		
		let funct = function () {
			let capturedText;
			eval ('capturedText=' + value);
			return capturedText;
		};

		attribute.getValue = function () {
			return funct.call(attribute.$scope);
		}
	}

	searchInTags (capsule) {
		if (!(capsule.template instanceof Element)) {
			return;
		}
		for (let i in this.etiquetas) {
			if (this.etiquetas[i].nombre.toUpperCase() == capsule.tagName) {
				capsule.tag = {
					tag: this.etiquetas[i],
					state: 0,
					listeners: []
				};
				capsule.toChange = true;
			}
		}
	}

	searchInAttributes (capsule) {
		if (!(capsule.template instanceof Element)) {
			return;
		}
		for (let i = 0; i < capsule.template.attributes.length; i++) {
			let nodeAttribute = capsule.template.attributes.item(i).nodeName;
			for (let j in this.atributos) {
				if (this.atributos[j].nombre == nodeAttribute) {
					capsule.attributes.push (this.atributos[j]);
					capsule.toChange = true;
				}
			}
		}
	}

	searchInNotation (capsule) {
		if (capsule.template instanceof Text) {
			if (this.maskedNotation (capsule.template.data)) {
				capsule.toChange = true;
				capsule.masked = true;
			}
		} else if (capsule.template instanceof Element) {
			for (let i = 0; i < capsule.template.attributes.length; i++) {
				let nodeAttribute = capsule.template.attributes.item(i);
				if (this.maskedNotation (nodeAttribute.nodeValue)) {
					capsule.toChange = true;
					if (!capsule.masked) {
						capsule.masked = [];
					}
					capsule.masked.push (nodeAttribute.nodeName);
				}
			}
		}
	}

	maskedNotation(string){
		var regex = /{{([^}]*)}}/g;
		
		var match = string.match(regex);
		if (match == null || match.length == 0) {
			return null;
		}

		return match;
	}

	enmascararNotacion(string){
		var regex = /{{([^}]*)}}/g;
		
		var newString = string.replace(regex, (exp, content, offset, text, s) => {
			return eval (content);
		});
		return newString;
	}
	
	searchInExisting (nodeElement) {
		this.searchInTags (nodeElement);
		this.searchInAttributes (nodeElement);
		this.searchInNotation (nodeElement);
	}

	//aplicar () {
	//	this.bootstrap (this.tree);
	//}

	_bootstrap (element, scope) {
		element.sweep(scope);
	}

	aplicar () {
		this._bootstrap (this.tree);
	}

	initialiceNode (capsule, scope) {
		capsule.element = capsule.template.cloneNode();
		if (!capsule.toChange) {
			capsule.initialiced = 2;
			return capsule;
		}
		capsule.initialiced = 2;
		if (capsule.tag) {
			capsule.initialiced = 1;
			let tag = new capsule.tag.tag.create();
			tag.app = this;
			tag.capsule = capsule;
			capsule.currentTag = tag;
			capsule.currentTag.controller();
			
			if (tag.tag.ruta) {
				if (tag.state == 2) { // with template
					let clone = tag._template.cloneNode();
					let child;
					while (child = clone.firstChild) {
						clone.removeChild(child);
						capsule.append (this.map (child));
						//nodeElement.children.push (this.map (child, nodeElement));
					}
					//if (nodeElement.currentTag.$init) {
					//	nodeElement.currentTag.$init();
					//}
				} else if (tag.state == 0) { // not looking
					tag.state = 1;
					tag.listeners.push (capsule);
					ajax.send (tag.tag.ruta, 'get', {})
					.then ((xhr) => {
						tag.state = 3;
						let div = document.createElement ('DIV');
						div.innerHTML = xhr.responseText;
						
						tag._template = div;
						
						for (let i in tag.listeners) {
							let clone = tag._template.cloneNode(true);
							let child;
							while (child = clone.firstChild) {
								clone.removeChild(child);
								tag.listeners[i].children.push (this.map (child));
							}
							if (tag.listeners[i].currentTag.$init) {
								tag.listeners[i].currentTag.$init();
							}
						}
						tag.state = 2;
						this.aplicar();
					})
					.catch (function (data) {
						tag.state = 3;
						capsule.initialiced = 2;
						throw amazonia.exceptions.notFoundRoute;
					});
				} else if (tag.state == 1) {
					if (tag.listeners.indexOf (nodeElement) < 0) {
						tag.listeners.push (nodeElement);
					}
				}
			} else {
				throw amazonia.exceptions.noRouteSelected;
			}
		}
		if (capsule.attributes.length) {
			capsule.currentAttributes = [];
			for (let i in capsule.attributes) {
				let attrib = new capsule.attributes[i].create();
				attrib.element = capsule.element;
				attrib.capsule = capsule;
				attrib.$scope = scope;
				attrib.name = capsule.attributes[i].nombre;
				attrib.app = this;
				attrib.value = capsule.element.getAttribute(capsule.attributes[i].nombre);
				capsule.currentAttributes[i] = attrib;
				if (capsule.currentAttributes[i].controller) {
					capsule.currentAttributes[i].controller();
				}
				//if (nodeElement.currentAttributes[i].$init) {
				//	nodeElement.currentAttributes[i].$init();
				//}
			}
		}
	}

    bootstrap (capsule, scope) {
		
		//if (!nodeElement.parent) {
		//	if (!nodeElement.hasParent) {
		//		throw amazonia.exceptions.inconsistentParentness;
		//	}
		//	if (!nodeElement.element) {
		//		nodeElement.element = nodeElement.template;
		//	}
		//} else {
			//if (nodeElement.hasParent) {
			//	throw amazonia.exceptions.inconsistentParentness2;
			//}
		if (capsule.initialiced == 0) {
			this.initialiceNode (capsule, scope);
		}
		if (capsule.initialice == 2) {

			//return;

		//if (!nodeElement.element && !nodeElement.evaluated) {
		//	this.initialiceNode (nodeElement, scope);
			
			if (capsule.parent.element == null) {
				
			}
			if (capsule.element instanceof Array) {
				for (let i in capsule.element) {
					capsule.parent.element.appendChild (capsule.element[i]);
				}
			} else {
				capsule.parent.element.appendChild (capsule.element);
			}
			if (capsule.tag && !capsule.tag._initialiced) {
				capsule.tag.$init();
			}
			if (capsule.attributes.length) {
				for (let i in capsule.attributes) {
					capsule.attributes[i].$init()
				}
			}
		}
		scope = capsule.currentTag || scope;
		this.render (capsule, scope);
		//}
		for (let i = 0; i < capsule.children.length; i++) {
			if (capsule.element && !capsule.evaluated) {
				this.bootstrap (capsule.children[i], scope);
			}
		}
	}

	render (nodeElement, scope) {
		if (nodeElement.masked) {
			if (nodeElement.masked instanceof Array) {
				for (let i in nodeElement.masked) {
					nodeElement.element.setAttribute (
						this.enmascararNotacion.call (scope, nodeElement.template.getAttribute (nodeElement.masked[i]))
					);
				}
			} else {
				nodeElement.element.data = this.enmascararNotacion.call (scope, nodeElement.template.data);
			}
		}
		if (nodeElement.tag) {
			if (nodeElement.currentTag.$render) {
				nodeElement.currentTag.$render();
			}
		}
		if (nodeElement.attributes.length) {
			for (let i in nodeElement.currentAttributes) {
				if (nodeElement.currentAttributes[i].$render) {
					nodeElement.currentAttributes[i].$render();
				}
			}
		}
	}

	appendNodeElement (nodeElement, child) {
		nodeElement.children.push (child);
		if (child.element) {
			nodeElement.element.appendChild (child.element);
		}
	}

	/*removeNodeElement (nodeElement, child) {
		
		let i = nodeElement.children.indexOf (child);
		if ( i !== -1 ) {
			nodeElement.children.splice( i, 1 );
		}
		try {
			nodeElement.element.removeChild (child.element);
		} catch (e) {
			console.log (e);
		}
		
	}*/

	removeNodeElement (capsule) {
		capsule.removed = true;
	}

	ocult (nodeElement) {
		if (!nodeElement.visible) {
			return;
		}
		nodeElement.visible = false;
		nodeElement.sweepChildren = false;
		nodeElement.children = [];
		if (nodeElement.parent.element) {
			nodeElement.parent.element.removeChild (nodeElement.element);
		}
		
	}

	show (capsule) {
		if (capsule.visible) {
			return;
		}
		capsule.visible = true;
		if (capsule.tag) {
			if (capsule.tag.state != 2) {
				capsule.initialiced = 2;
				return;
			}
			capsule.generateNode();
		} else {
			capsule.generateNode();
		}
		capsule.sweepChildren = true;
		capsule.initialiced = 3;
	}


	convertNodeToNone (nodeElement) {
		nodeElement.visible = false;
		//nodeElement.evaluated = true;
		//if (nodeElement.element instanceof Node) {
		//	nodeElement.parent.element.removeChild (nodeElement.element);
		//}
		
		//nodeElement.children = [];
		//nodeElement.children = [];
		//let r = (n) => { n.element = null; n.children.forEach ((x) => r(x)) };
		//r(nodeElement);
		//nodeElement.element = null;
	}

	/*createNodeElement (node, parent) {
		let newNodeElement = {
			template: node,
			element: null,
			type: typeof (node),
			parent: parent,
			children: [],
			hasParent: !!node.parentNode,
			element: null,
			attributes: [],
			toChange: false,
			masked: null
		}
		this.searchInExisting (newNodeElement);
		return newNodeElement;
	}*/

	cloneNodeElement (capsule, deep) {
		if (!deep) {
			throw 'Not implemented';
			//return this.createNodeElement (nodeElement.template, nodeElement.parent);
		} else {
			let rep = (amnode, parent) => {
				let clone = new Capsule (amnode.template.cloneNode(false));
				clone.transclude = amnode.transclude
				clone.app = this;
				this.searchInExisting (clone);
				if (parent) {
					parent.append (clone);
				}
					
				for (let i in amnode.children) {
					rep (amnode.children[i], clone);
				}
				return clone;
			}
			return rep (capsule);
		}
	}
	
	/*map (node, parent) {
		let nodeElement = this.createNodeElement (node, parent);
		let children = [];
		while (node.hasChildNodes()) {
			children.push (node.firstChild);
            node.removeChild(node.firstChild);
		}
		for (let i = 0; i < children.length; i++) {
			nodeElement.children.push(this.map (children[i], nodeElement));
		}
		return nodeElement;
	}*/

	

	map (node, app) {
		let children = [];
		while (node.hasChildNodes()) {
			children.push (node.firstChild);
			node.removeChild (node.firstChild);
		}
		let capsule = new Capsule (node);
		
		if (app) {
			capsule.app = app;	
		}
		this.searchInExisting (capsule);
		if (capsule.tag) {
			capsule.tranclude = children;
			return capsule;
		}
		
		for (let i = 0; i < children.length; i++) {
			let child = this.map (children[i], app);
			capsule.append (child);
		}
		return capsule;
	}
    
    capture (node) {
		this.tree = this.map (node, this);
    }

	crear () {
		if (this._controlador)
            this._controlador.apply (this);
		this.capture (document.getElementsByTagName ('body')[0]);
		//console.log (this.tree);
	}

	encender () {
		document.addEventListener("DOMContentLoaded" , (event) => {
			this.crear ();
			this.aplicar();
			//v.aplicar ();
		});
	}
}

